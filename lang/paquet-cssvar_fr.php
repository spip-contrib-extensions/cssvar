<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = [
	// C
	'cssvar_description' => 'Ce plugin permet de déclarer des variables CSS depuis le PHP, qui seront insérées ensuite automatiquement dans l\'en-tête des pages.	
	Il est possible d\'insérer des variables communes à toutes les pages, mais aussi spécifiquement sur des pages précises. Le plugin pré-calcule aussi certaines variables pratiques pour les couleurs, en décomposant les éléments HSL et RVB.',
	'cssvar_slogan' => 'Injecter facilement des variables CSS dans les pages',
	'cssvar_nom'    => 'Variables CSS'
];
