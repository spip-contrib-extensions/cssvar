<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function cssvar_insert_head_css($flux) {
	// La définition pour toutes les pages statiquement
	$definition = cssvar_definition();
	
	if ($styles = cssvar_generer_styles($definition)) {
		$flux.= "<style type='text/css'>\n" . $styles . "\n</style>\n";
	}
	
	return $flux;
}

function cssvar_affichage_final($flux) {
	include_spip('public/assembler');
	$contexte = cssvar_contextualiser_salement();
	$definition = cssvar_definition($contexte);
	
	if ($styles = cssvar_generer_styles($definition)) {
		$pos_head = strpos($flux, '</head>');
		$flux = substr_replace($flux, "<style type='text/css'>\n" . $styles . "\n</style>\n", $pos_head, 0);
	}
	
	return $flux;
}

function cssvar_generer_styles($definition) {
	$styles = '';
	
	// On parcourt chaque sélecteurs CSS
	foreach ($definition as $selecteur => $variables) {
		// On ne continue que s'il y a des variables
		if (!empty($variables) and is_array($variables)) {
			$styles .= $selecteur . " {\n";
			foreach ($variables as $variable => $valeur) {
				// On ajoute telle quelle
				$styles .= "\t$variable: $valeur;\n";
			}
			$styles .= "}";
		}
	}
	
	return $styles;
}

function cssvar_definition($contexte = null) {
	$definition = array();
	
	// Si aucun contexte, c'est pour des styles statiques à toutes les pages
	// TODO : on pourrait injecter ici la couleur principale choisie dans l'identité du site, ce qui permettrait de personnaliser un thème sans autre config en plus
	if (empty($contexte)) {
		$definition[':root'] = array(
			//~ '--color-primary' => '#123456', // test
		);
	}
	
	// On passe dans un pipeline
	$definition = pipeline(
		'cssvar',
		array(
			'args' => $contexte,
			'data' => $definition,
		)
	);
	
	// On génère peut-être des variantes magiquement (pour > 4.0 uniquement)
	include_spip('inc/filtres_images_mini');
	if (!empty($definition) and function_exists('couleur_hex_to_hsl')) {
		
		foreach ($definition as $selecteur => $variables) {
			// On ne continue que s'il y a des variables
			if (!empty($variables) and is_array($variables)) {
				foreach ($variables as $variable => $valeur) {
					// SI la variable est bien une variable ET que c'est une couleur hexadécimale, on génère des variantes de ouf
					if (strpos($variable, '--') === 0 and strpos($valeur, '#') === 0 and in_array(strlen($valeur), array(4, 7))) {
						include_spip('inc/config');
						
						$definition[$selecteur]["$variable--h"] = couleur_hex_to_hsl($valeur, 'h');
						$definition[$selecteur]["$variable--s"] = couleur_hex_to_hsl($valeur, 's');
						$definition[$selecteur]["$variable--l"] = couleur_hex_to_hsl($valeur, 'l');
						$definition[$selecteur]["$variable--r"] = couleur_hex_to_rgb($valeur, 'r');
						$definition[$selecteur]["$variable--g"] = couleur_hex_to_rgb($valeur, 'g');
						$definition[$selecteur]["$variable--b"] = couleur_hex_to_rgb($valeur, 'b');
						// Luminosité réellement perçue, selon la méthode accessible du W3C
						$definition[$selecteur]["$variable--lightness"] = (
							$definition[$selecteur]["$variable--r"] * 0.2126
							+ $definition[$selecteur]["$variable--g"] * 0.7152
							+ $definition[$selecteur]["$variable--b"] * 0.0722
						) / 255;
						// Sombre ou clair ? TODO seuil configurable
						$definition[$selecteur]["$variable--is-dark"] = (int)!($definition[$selecteur]["$variable--lightness"] > lire_config('cssvar/luminosite_seuil', 0.6));
						$definition[$selecteur]["$variable--is-bright"] = (int)($definition[$selecteur]["$variable--lightness"] > lire_config('cssvar/luminosite_seuil', 0.6));
					}
				}
			}
		}
	}
	
	return $definition;
}

function cssvar_contextualiser_salement() {
	include_spip('base/objets');
	$contexte = array(
		'status' => isset($GLOBALS['page']['status']) ? $GLOBALS['page']['status'] : 200,
	);
	
	// Si c'est une page=
	if (isset($GLOBALS['page']['contexte']['page']) and $page = $GLOBALS['page']['contexte']['page']) {
		$contexte += array(
			'type-page' => $page,
			'page' => $page,
		);
	}
	// Sinon si c'est un objet
	elseif (isset($GLOBALS['page']['contexte']['type-page']) and $objet = $GLOBALS['page']['contexte']['type-page']) {
		$cle_objet = id_table_objet($objet);
		$id_objet = $GLOBALS['page']['contexte'][$cle_objet];
		$contexte += array(
			'type-page' => $objet,
			'objet' => $objet,
			'id_objet' => $id_objet,
			$cle_objet => $id_objet,
			'cle_objet' => $cle_objet,
		);
	}
	// Sinon c'est très sûrement la page d'accueil, donc en dur, comme dans le code de assembler
	else {
		$contexte += array(
			'type-page' => 'sommaire',
			'page' => 'sommaire',
		);
	}
	
	return $contexte;
}
