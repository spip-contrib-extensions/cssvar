# Variables CSS

Ce plugin permet de déclarer des variables CSS depuis le PHP, qui seront insérées ensuite automatiquement dans l'entête des pages.

Il est possible d'insérer des variables communes à toutes les pages, mais aussi spécifiquement sur des pages précises.

Le plugin pré-calcule aussi certaines variables pratiques pour les couleurs, en décomposant les éléments HSL et RVB.

## Insérer des variables

Pour insérer ses propres variables, il faut utiliser le pipeline `cssvar` et ajouter dans la clé `data` des tableaux de variables dont la clé racine sera le sélecteur CSS :
``` php
$flux['data'][':root'] = [
	'--variable' => 'valeur',
	'--color-primary' => '#112233'
];
$flux['data']['.composition_chose'] = [
	'--color-primary' => '#445566';
];
```

### Insérer des variables communes sur toutes les pages

S'il n'y a rigoureusement aucun argument de contexte dans le pipeline, alors c'est qu'on est au bon endroit pour insérer sur toutes les pages dans `insert_head_css`.

``` php
function monplugin_cssvar($flux) {
	// Sur toutes les pages
	if (empty($flux['args'])) {
		$flux['data'][':root'] = …
	}
	
	return $flux;
}
```

### Insérer des variables sur des pages précises

Le plugin permet aussi d'insérer dans `affichage_final` sur des pages précises en donnant un contexte pour les repérer.

``` php
function monplugin_cssvar($flux) {
	// Sur la page d'accueil uniquement
	if (!empty($flux['args']['page']) and $flux['args']['page'] == 'sommaire') {
		$flux['data'][':root'] = …
	}
	
	// Sur la patate 3
	if (!empty($flux['args']['objet']) and $flux['args']['objet'] == 'patate' and $flux['args']['id_objet'] == 3) {
		$flux['data'][':root'] = …
	}
	
	return $flux;
}
```

## Variables utiles pour les couleurs

Dès que le plugin détecte une variable qui est une **couleur hexadécimale**, il va générer de multiples variables supplémentaires pour toutes les composantes de la couleur.

``` css
--ma-couleur: #XXX;
--ma-couleur--h: …;
--ma-couleur--s: …;
--ma-couleur--l: …;
--ma-couleur--r: …;
--ma-couleur--g: …;
--ma-couleur--b: …;
--ma-couleur--lightness: …; /* Luminosité W3C */
--ma-couleur--is-dark: 0 ou 1;
--ma-couleur--is-bright: 0 ou 1;
```

La luminosité et dark et bright permettent notamment dans vos styles, de changer dynamiquement la couleur de texte (pour du foncé ou du clair) suivant la couleur du fond, ce qui permet de faire des thèmes dynamiques dont les couleurs sont configurables par les admins en ayant toujours un contraste de lecture accessible.
